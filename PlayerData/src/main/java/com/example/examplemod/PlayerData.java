package com.example.examplemod;

import org.lwjgl.glfw.GLFW;                  // Needed for GLFW_ symbols.
import com.mojang.logging.LogUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.core.Direction;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.player.Player;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.event.entity.player.PlayerEvent;
import net.minecraftforge.event.AttachCapabilitiesEvent;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.CapabilityManager;
import net.minecraftforge.common.capabilities.CapabilityToken;
import net.minecraftforge.common.capabilities.ICapabilitySerializable;
import net.minecraftforge.common.capabilities.RegisterCapabilitiesEvent;
import net.minecraftforge.common.util.INBTSerializable;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraft.nbt.*;
import net.minecraftforge.fml.common.Mod;    // Needed for @Mod.
import net.minecraftforge.fml.LogicalSide;
import net.minecraftforge.fml.util.thread.EffectiveSide;


@Mod.EventBusSubscriber(modid = ExampleMod.MODID)
public class PlayerData implements INBTSerializable<CompoundTag>
{
	private static final Capability<PlayerData> PLAYER_DATA_CAPABILITY = CapabilityManager.get(new CapabilityToken<>(){});
    private static final ResourceLocation RESOURCE = new ResourceLocation(ExampleMod.MODID, "player_data");

    private final Player player;

    private int login_count;


    public static void registerCaps(RegisterCapabilitiesEvent event)
    {
        event.register(PlayerData.class);
    }

    @SubscribeEvent
	public static void onPlayerCloneEvent(PlayerEvent.Clone event)
    {
        Player     new_player = event.getEntity();
        Player     old_player = event.getOriginal();
        PlayerData new_data   = null;
        PlayerData old_data   = null;
        
        new_data = PlayerData.get(new_player);
        old_player.reviveCaps();
        old_data = PlayerData.get(old_player);

        if ((null == new_data) || (null == old_data))
        {
            LogUtils.getLogger().error("Capabilities missing when cloning player " + new_player.getName().getString() + ".");
        }
        else
        {
            new_data.copyFrom(old_data, event.isWasDeath());
        }

        old_player.invalidateCaps();
    }

    @SubscribeEvent
    public static void onLogin(PlayerEvent.PlayerLoggedInEvent event)
    {
        Player player = event.getEntity();

        if (LogicalSide.SERVER == EffectiveSide.get())
        {
            PlayerData data = PlayerData.get(player);

            data.login_count++;
            Minecraft.getInstance().player.displayClientMessage(Component.literal("You have logged in " + data.login_count + " times."), false);
        }
    }

    // We need to template this function to a generic type, so "Entity" rather than "Player" and then check that
    // the entity is a player. To attach capabilities to a different type of object, template the function to
    // that type.
    @SubscribeEvent
    public static void onAttachCapability(AttachCapabilitiesEvent<Entity> event)
    {
		if (event.getObject() instanceof Player)
        {
            Player player = (Player)event.getObject();

			event.addCapability(RESOURCE, new PlayerData.Provider(player));
        }
	}

	public PlayerData(Player player)
    {
		this.player = player;
        this.login_count = 0;
    }

    public static PlayerData get(Player player)
    {
        PlayerData                result;
        LazyOptional<PlayerData>  lo     = player.getCapability(PLAYER_DATA_CAPABILITY);

        if (lo.isPresent()) {
            result = lo.resolve().get();
        } else {
            // This should never happen, probably ought to log a failure.
            result = null;
        }

        return result;
	}

    public void copyFrom(PlayerData src, boolean respawn)
    {
        this.login_count = src.login_count;

        if (respawn)
        {
            LogUtils.getLogger().info(this.player.getName().getString() + " respawned.");
            // If you want to penalise the character for dying in some way, do it here.
        }
        else
        {
            LogUtils.getLogger().info (this.player.getName().getString() + " cloned.");
            // TODO: Sync to client. Not covered in this example.
        }
    }

    @Override
	public CompoundTag serializeNBT()
    {
		CompoundTag values = new CompoundTag();

        values.putInt("login_count", login_count);

		return values;
	}

    @Override
	public void deserializeNBT(CompoundTag values)
    {
        login_count = values.getInt("login_count");
    }

	public static class Provider implements ICapabilitySerializable<CompoundTag>
    {
        private final PlayerData               data;
        private final LazyOptional<PlayerData> lazy_optional;


		public Provider(Player player)
        {
            data = new PlayerData(player);
            lazy_optional = LazyOptional.of(() -> data);
		}

		@Override
		public <T> LazyOptional<T> getCapability(Capability<T> capability, Direction side)
        {
            return PLAYER_DATA_CAPABILITY.orEmpty(capability, lazy_optional);
		}

		@Override
		public CompoundTag serializeNBT()
        {
			return data.serializeNBT();
		}

		@Override
		public void deserializeNBT(CompoundTag nbt)
        {
			data.deserializeNBT(nbt);
		}
	}
}


