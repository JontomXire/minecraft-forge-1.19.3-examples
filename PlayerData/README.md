## Player data

This example counts how many times the player has logged in and reports the result to the user.

There is no synchronisation between server and client side. To implement that requires additional networking capability which is not covered by this example.

This example uses the Capabilities functionality. In this example, all the code is in a single class. Normally it might be distributed between different classes.


