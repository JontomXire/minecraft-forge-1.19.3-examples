package com.example.examplemod;

import org.lwjgl.glfw.GLFW;                  // Needed for GLFW_ symbols.
import com.mojang.blaze3d.platform.InputConstants;
import net.minecraft.client.Minecraft;
import net.minecraft.client.KeyMapping;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.MutableComponent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.fml.common.Mod;    // Needed for @Mod.
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.client.event.RegisterKeyMappingsEvent;
import net.minecraftforge.client.settings.KeyConflictContext;
import net.minecraftforge.event.TickEvent;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.eventbus.api.SubscribeEvent;


@Mod.EventBusSubscriber(modid = ExampleMod.MODID, bus = Mod.EventBusSubscriber.Bus.MOD, value = Dist.CLIENT)
public class ClientEventHandler
{
    private static KeyMapping do_something_key;


    private ClientEventHandler(){} // No instances!


    @SubscribeEvent
    public static void registerBindings(RegisterKeyMappingsEvent event)
    {
        // If the default key is one already in use, then the key mapping will not trigger.
        do_something_key = new KeyMapping("key.examplemod.do_something",
                                          KeyConflictContext.IN_GAME,
                                          InputConstants.Type.KEYSYM,
                                          GLFW.GLFW_KEY_N,
                                          "key.categories.examplemod");
        event.register(do_something_key);
    }

    public static void onClientTick(TickEvent.ClientTickEvent event)
    {
        // Only call code once as the tick event is called twice every tick.
        if (event.phase == TickEvent.Phase.END)
        {
            while (do_something_key.consumeClick())
            {
                Minecraft.getInstance().player.displayClientMessage(Component.literal("You pressed the key!"), true);
            }
        }
    }
}

