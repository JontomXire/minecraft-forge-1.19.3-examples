# Minecraft Forge 1.19.3 examples

## Useful links

- https://docs.minecraftforge.net/en/1.19.x/
- https://forge.gemwire.uk/wiki/Main_Page
- https://nekoyue.github.io/ForgeJavaDocs-NG/


## General comments

I am familiar, although not expert with Java, but I am also an extremely experienced software engineer. I have been doing it for over 25 years. It took me an entire morning just to do a single key mapping and get it to work. This repository is to provide working code examples to save others the pain and trouble that I suffered.

The reason I struggled so is that the Forge documentation is rather poor. The failures in the forge documentation are as follows:

### Bugs in the code snippets

An event handler needed to be static given the apparent, but not explicitly stated, context in which it appeared. If the code snippets were actually tested, or if there was more detail on the page about event handling about when and why event handlers need to be static, given the different Forge event handling mechanisms, then such problems would not occur.

### Lack of detailed documentation for classes

I have been told in the forums and bug report conversations that because Microsoft change their code and do not publish an API, that the only way to get detailed documentation is using Maven or something to reverse engineer the code.

The second "useful link" above is a web page where someone does the reverse engineering and provides the documentation. It should be more than possible to make this a fully automated feature of the Forge website. It would be extremely useful and avoid us all duplicating the same work.

However the classes and functions I was having difficulty with were all 100% Forge classes and functions. There is absolutely no reason why they cannot doxygen their code and, in the process, provide better detail of how the functions work, restrictions on use, etc. etc.

### Failure to detail the packages to be imported

If you know the packages that contain a class, then you can find the documentation for them and get more background information on the functions and classes you are using. Finding a package given a class name is not impossible, but is much harder, especially for some names which are used in multiple different packages.

## Overview of the examples

The examples are all based on modding work I have done myself. If you are trying to do something I have never done, then there won't be any example code here.

Each example is, as best as I can make it, a minimum code example. That said, I am trying to stick to best practices and a good overall design. Unfortunately I am struggling to reduce code to only import packages that are strictly necessary, so some spurious ones may be imported.

To build the example, if you wish to do so, copy the following files and folders from the Forge MDK:

- gradle/
- gradle.properties
- gradlew
- gradlew.bat
- settings.gradle

## List of examples
### Key mapping

A simple example of mapping a single key. When the key is pressed, a string of debug is output to the debug.log file for the Minecraft installation.

For this example to work, there must not be a collision of key assignment.

### Player data

A simple example of adding a single additional per player attribute.


## Please help

If you see a code example that could be done better with a different code structure, for example, please let me know. I have a full time job and a full time family and a number of software projects that have fallen by the wayside due to lack of time, but I will do what I can to maintain this repository.

Also, if you want me to add a code example of your own, I am more than happy to do so.


